package com.andrius.meteoapp.model;

import com.andrius.meteoapp.utils.BreakLines;
import com.andrius.meteoapp.utils.IOService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Code created by Andrius on 2020-08-21
 */
public class Meteo {
    private Place place;
    private String forecastType;
    private String forecastCreationTimeUtc;
    private List<ForecastTimestamp> forecastTimestamps;

    public String getForecastType() {
        return forecastType;
    }

    public String getForecastCreationTimeUtc() {
        return forecastCreationTimeUtc;
    }

    public Place getPlace() {
        return place;
    }

    public List<ForecastTimestamp> getSortedForecastTimestampsByDate() {
        return this.forecastTimestamps.stream().sorted().collect(Collectors.toList());
    }

    public List<ForecastTimestamp> getAllForecastsFromNow() {
        Date date = new Date();
        return getSortedForecastTimestampsByDate().
                stream().
                filter(forecastTimestamp -> forecastTimestamp.
                        getForecastTimeUtc().
                        compareTo(date) > 0).
                collect(Collectors.toList());
    }

    public int countAvailableForecasts() {
        return getAllForecastsFromNow().size();
    }

    public void printTemperaturesForSelectedPeriod(int hours) {
        IOService.message("\nTemperatures info up to " + hours + " hours:");
        String line = BreakLines.lineGenerator(hours);
        StringBuilder time = new StringBuilder("Time:        | ");
        StringBuilder temperatures = new StringBuilder("Temperature: | ");
        StringBuilder wind = new StringBuilder("Wind speed:  | ");
        StringBuilder humidity = new StringBuilder("Humidity:    | ");
        for (int i = 0; i < hours; i++) {
            time.append(getAllForecastsFromNow().get(i).getSimpleTimeFormat()).append("  | ");
            temperatures.append(getAllForecastsFromNow().get(i).getAirTemperature()).append("°C | ");
            wind.append(getAllForecastsFromNow().get(i).getWindSpeed()).append("m/s   | ");
            humidity.append(getAllForecastsFromNow().get(i).getRelativeHumidity()).append("%    | ");
        }
        System.out.printf("%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s%n%s",
                line, time, line, temperatures,
                line, wind, line, humidity, line);
    }

    public void printForecastForExactHour(int hour) {
        System.out.printf("%s%n%n", getAllForecastsFromNow().get(hour - 1).toString());
    }
}
