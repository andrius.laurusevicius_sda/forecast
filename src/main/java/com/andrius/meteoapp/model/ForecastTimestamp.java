package com.andrius.meteoapp.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Code created by Andrius on 2020-08-21
 */
public class ForecastTimestamp implements Comparable<ForecastTimestamp> {

    private Date forecastTimeUtc;
    private double airTemperature;
    private int windSpeed;
    private int seaLevelPressure;
    private int relativeHumidity;
    private String conditionCode;

    public Date getForecastTimeUtc() {
        return forecastTimeUtc;
    }

    public String getSimpleTimeFormat() {
        return new SimpleDateFormat("kk:mm").format(this.forecastTimeUtc);
    }

    public String getSimpleFullDateTimeFormat() {
        return new SimpleDateFormat("kk:mm dd-MM-yyyy").format(this.forecastTimeUtc);
    }

    public double getAirTemperature() {
        return airTemperature;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public int getRelativeHumidity() {
        return relativeHumidity;
    }

    public String getConditionCode() {
        return this.conditionCode;
    }

    @Override
    public String toString() {
        return String.format("Time: %s%nTemperature %.1f°C%nWind speed: %dm/s%nPressure: %dhPa%nHumidity: %d%%%nConditions: %s",
                getSimpleFullDateTimeFormat(), this.airTemperature, this.windSpeed, this.seaLevelPressure, this.relativeHumidity,
                this.getConditionCode());
    }

    @Override
    public int compareTo(ForecastTimestamp forecastTimestamp) {
        return this.forecastTimeUtc.compareTo(forecastTimestamp.getForecastTimeUtc());
    }
}
