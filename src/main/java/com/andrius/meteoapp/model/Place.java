package com.andrius.meteoapp.model;

import java.util.Map;

/**
 * Code created by Andrius on 2020-08-21
 */
public class Place {
    private String code;
    private String name;
    private Map<String, Double> coordinates;

    public String getCode() {
        return code;
    }

    public Map<String, Double> getCoordinates() {
        return coordinates;
    }

    private String getAllCoordinatesWithValues() {
        String[] coord = new String[getCoordinates().size()];
        int i = 0;
        for (Map.Entry<String, Double> value : this.coordinates.entrySet()) {
            coord[i] = String.valueOf(value.getValue());
            i++;
        }
        return String.format("Coordinates - %s : %s", coord[0], coord[1]);
    }

    private String tagLineWithCityName() {
        return String.format("%s - forecast from Meteo.lt API:", this.name);
    }

    @Override
    public String toString() {
        return String.format("%s%n%s", tagLineWithCityName(), getAllCoordinatesWithValues());
    }
}
