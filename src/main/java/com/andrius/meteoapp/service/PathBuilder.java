package com.andrius.meteoapp.service;

import com.andrius.meteoapp.utils.IOService;

/**
 * Code created by Andrius on 2020-08-25
 */
public class PathBuilder {

    private static final String PATH_START = "https://api.meteo.lt/v1/places/";
    private static final String PATH_END = "/forecasts/long-term";

    public static String getPath() {
        IOService.message("\nEnter city name (accepts Lithuanian letters):");
        String input = IOService.getInputWithoutLtLettersAndSpecialCharacters();
        return String.format("%s%s%s", PATH_START, input, PATH_END);
    }
}

