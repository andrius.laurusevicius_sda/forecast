package com.andrius.meteoapp.service;

import com.andrius.meteoapp.model.Meteo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Code created by Andrius on 2020-08-21
 */
public class Parser {

    Gson gson;

    public Parser() {
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    public Meteo read() {
        return gson.fromJson(CallToApi.getResponseFromApi(), Meteo.class);
    }
}
