package com.andrius.meteoapp.service;

import com.andrius.meteoapp.utils.ResponseTimeDate;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * Code created by Andrius on 2020-08-25
 */
public class CallToApi {

    private static String getHttpResponse() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(PathBuilder.getPath()))
                .build();

        HttpResponse<String> response = null;
        long start = System.nanoTime();
        try {
            response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            System.err.println("Connection error.");
        }
        long end = System.nanoTime();
        ResponseTimeDate.printResponseTimeInformation(start, end);
        return response.body();
    }

    public static String getResponseFromApi() {
        String response = getHttpResponse();
        while (response.contains("error")) {
            System.out.println("\nWrong city name! Enter again.");
            response = getHttpResponse();
        }
        return response;
    }
}
