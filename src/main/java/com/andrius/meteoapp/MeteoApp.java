package com.andrius.meteoapp;

import com.andrius.meteoapp.model.Meteo;
import com.andrius.meteoapp.service.Parser;
import com.andrius.meteoapp.utils.IOService;

/**
 * Code created by Andrius on 2020-08-25
 */
public class MeteoApp {

    public void run() {
        printWelcomeMessage();
        do {
            Meteo meteo = new Parser().read();
            printLocationInfoandForecast(meteo);
            printForeCastPeriodFromUserInput(meteo);
            printQuestionForUserInput(meteo);
            printForecastForSelectedHour(meteo);
        } while (getUserAnswer());
    }

    private void printWelcomeMessage() {
        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n" +
                "Forecast in selected city:" +
                "\n=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
    }

    private void printLocationInfoandForecast(Meteo meteo) {
        System.out.printf("%n%s%nForecast created at: %s%n",
                meteo.getPlace(), meteo.getForecastCreationTimeUtc());
    }

    public void printForeCastPeriodFromUserInput(Meteo meteo) {
        IOService.message("\nSelect forecast interval (recommended from 2 to 10 hours).");
        meteo.printTemperaturesForSelectedPeriod(IOService.getIntegerInput());
    }

    private void printQuestionForUserInput(Meteo meteo) {
        System.out.printf("%n%nEnter hour value, to receive forecast after xx hours." +
                "\nWe have forecast up to %d hours.\n", meteo.countAvailableForecasts());
    }

    private void printForecastForSelectedHour(Meteo meteo) {
        meteo.printForecastForExactHour(IOService.getIntegerInput());
    }

    private boolean getUserAnswer() {
        IOService.getInputWithoutLtLettersAndSpecialCharacters();
        IOService.message("Wanna repeat, enter - Yes, to finish - No.");
        String answer = IOService.getInputWithoutLtLettersAndSpecialCharacters();
        return answer.equalsIgnoreCase("yes");
    }
}
