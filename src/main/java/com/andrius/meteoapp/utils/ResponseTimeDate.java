package com.andrius.meteoapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Code created by Andrius on 2020-08-26
 */
public class ResponseTimeDate {

    public static void printResponseTimeInformation(long start, long end) {
        double time = calculateResponseTime(start, end);
        System.out.printf("%n> Request time %s%n" +
                "> Server response time %.2fs%n%s", getCurrentTime(), time, BreakLines.SMALL_LINE);
    }

    public static String getCurrentTime() {
        Date date = new Date();
        return new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ").format(new Date());
    }

    public static double calculateResponseTime(long start, long end) {
        return (double) (end - start) / 1_000_000_000;
    }
}
