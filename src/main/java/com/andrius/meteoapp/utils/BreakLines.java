package com.andrius.meteoapp.utils;

/**
 * Code created by Andrius on 2020-08-25
 */
public class BreakLines {

    public static final String SMALL_LINE = "----------------------------------------";

    public static String lineGenerator(int hours) {
        return "" + "-".repeat(Math.max(0, 14 + (9 * hours)));
    }
}
