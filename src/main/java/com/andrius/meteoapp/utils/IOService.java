package com.andrius.meteoapp.utils;

import java.util.Scanner;

/**
 * Code created by Andrius on 2020-08-21
 */
public class IOService {

    private static Scanner scanner = new Scanner(System.in);

    private IOService() {
    }

    public static int getIntegerInput() {
        int intValue;
        try {
            intValue = Integer.parseInt(scanner.next());
        } catch (NumberFormatException e) {
            System.out.println("Only numbers allowed! Enter again.");
            return getIntegerInput();
        }
        return intValue;
    }

    public static void message(String message) {
        System.out.println(message);
    }

    public static String getInputWithoutLtLettersAndSpecialCharacters() {
        return scanner.nextLine().toLowerCase().
                replace(" ", "").
                replace("ą", "a").
                replace("ę", "e").
                replace("č", "c").
                replace("ė", "e").
                replace("į", "i").
                replace("š", "s").
                replace("ų", "u").
                replace("ū", "u").
                replace("ž", "z").
                replaceAll("[^a-zA-Z0]+", "");
    }
}
