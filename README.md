## Forecast App by Meteo.lt API

### Content

[1. How to run program](#how-to-run-this-programm)

[2. Description](#description)

[3. Screenshots](#screenshots)

[4. Keywords](#keywords)

### How to run this program:

To run this program you will need to clone the project _[git clone https://gitlab.com/andrius.laurusevicius_sda/forecast.git]_. Open this project in your IDE and run Main (Main.java) class.

### Description

The app makes a live requests to Meteo.lt API and receives all forecast data from server. Main features of the app:

- building custom url path from user input to get live data from API;
- accepts Lithuanian and English version of city names;
- exception catching if city not found;
- http request response time calculation;
- dynamic table which shows selected period of forecast; 
- detailed version of forecast for selected hour;

### Screenshots

![alt text](https://i.ibb.co/PtTVHkT/forecast-meteo-app.png)

### Keywords:

Collections API, Gson, JSON, HttpRequest, Comparable, Comparator, Lambda expressions, Maven project, Exceptions, Composition.